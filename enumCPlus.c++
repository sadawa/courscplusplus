#include <iostream>


// Create Enum Day
enum Day {
    Monday = 0 ,
    Tuesday = 1 ,
    Wednesday = 2 ,
    Thursday = 3 ,
    Friday = 4 ,
    Saturday = 5 ,
    Sunday =6
};

int main() {
   
   // Use enum Day
   Day day = Wednesday;
   std::cout << "Day of the week is: " << day << std::endl;
   

    return 0;
}