#include <iostream>

int searchArray(int array[],int size , int element);


int main(){
    // Use Arrays 
    int array[5] = {1, 2, 3, 4, 5};
    
    // Accessing array elements
    std::cout << "Array elements: ";
    for (int i = 0; i < 5; i++) {
        std::cout << array[i] << " ";
    }
    std::cout << std::endl;
    
    // Updating array elements
    array[2] = 10;
    std::cout << "Updated array elements: ";
    for (int i = 0; i < 5; i++) {  
        std::cout << array[i] << " ";
    }
    std::cout << std::endl;
    
    // use array brand cars
    std::string cars[5] = {"Toyota", "Honda", "Ford", "Tesla"};
    std::cout << "Brand of cars: " + cars[3] + "\n";

    // use fill() in array
    std::fill(array, array + 5, 0);
    std::cout << "Filled array elements:  \n";
    for (int i = 0; i < 5; i++) {
        std::cout << array[i] << " \n";
    }
    std::cout << std::endl;


    
    // use foreach in array
    int numbers[] = {1, 2, 3, 4, 5};
    for (int num : numbers) {
        std::cout << num << " \n";
    }


    // search an array for an element with function
    int nuumbers[] = {1, 2, 3, 4, 5 , 6 , 7, 8, 9, 10, 11 , 12, 13 , 14, 15, 16, 17};
    int size = sizeof(nuumbers) / sizeof(nuumbers[0]);
    int index;
    int myNum;

    std::cout << "Enter the number to search:  \n";
    std::cin >> myNum;

    index = searchArray(nuumbers, size, myNum);

    if(index != -1){
        std::cout << "Element found at index: " << index << std::endl;
    } else {
        std::cout << "Element not found in array" << std::endl;
    }


    return 0;
}

// Function to search an array for an element

int searchArray(int array[], int size, int element) {
    for (int i = 0; i < size; i++) {
        if (array[i] == element) {
            return i;
        }
    }
    return -1; // Element not found
}