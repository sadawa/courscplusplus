#include <iostream>

int main() {
    //for methode

    int sum = 0;

    for(int i = 0; i <= 10; i++) {
        std::cout << i << " \n";
        sum += i;
    }
    
    std::cout << "The sum of numbers from 0 to 10 is: " << sum << std::endl;

    return 0;
}