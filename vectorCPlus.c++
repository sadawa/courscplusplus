#include <iostream>
#include <vector>

int main() {

    // declare and initialize a vector of integers
    std::vector<int> numbers = {1, 2, 3, 4, 5};
    numbers.push_back(7);
    
    // print numbers [3]
    std::cout << numbers[3] << std::endl;
    std::cout << numbers.capacity() << std::endl;
    return 0;
}