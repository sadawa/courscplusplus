#include<iostream>

int main() {
    //logical operator with && operator
    int temp;

    std::cout <<  "Enter the temperature : \n";
    std::cin >> temp;

    if(temp >= 20 && temp <= 25){
        std::cout << "The temperature is normal\n";
    } 
    else if(temp > 25 && temp <= 30){
        std::cout << "The temperature is warm\n";
    }
    else{
        std::cout << "The temperature is not normal\n";
    }
    //---- 
    //logical operator with || operator
    int number;
    std::cout <<  "Enter the number : \n";
    std::cin >> number;
    
    if(number % 2 == 0 || number % 3 == 0){
        std::cout << "The number is divisible by 2 or 3\n";
    } 
    else{
        std::cout << "The number is not divisible by 2 or 3\n";
    }

    //--------
    //logical operator with ! operator
    int age;
    std::cout <<  "Enter the age : \n";
    std::cin >> age;
    
    if(age >= 18 && age <= 65 && age != 30){
        std::cout << "The person is eligible for voting\n";
    }
    else{
        std::cout << "The person is not eligible for voting\n";
    }
            
   
    return 0;
}