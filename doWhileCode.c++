#include <iostream>

int main() {
    //do while methode 

    int number;

  do {
    std::cout << "Please enter a positive number: ";
    std::cin >> number;
    }  while(number < 0);

    std::cout << "The entered number is: " << number << std::endl;

    return 0;
}