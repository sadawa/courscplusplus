#include<iostream>

int main() {
   //User input
   std::string name ;
   int age ;
   std::cout << "Enter your name: ";
//    std::cin >> name;
   //or use getline
   //std::cout << "Enter your name: ";
   std::getline(std::cin >> std::ws, name);
   std::cout << "Enter your age: ";
   std::cin >> age;

    //Output
   std::cout << "Hello, " << name << std::endl;
   std::cout << "You are " << age << " years old." << std::endl;
   return 0;
}