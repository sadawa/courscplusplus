#include <iostream>

//  Create OOP Human 
class Human {
    public:
    std::string name;
    int age;
    std::string gender;
    

    void eat(){
        std::cout << name << " is eating." << std::endl;
    };
    void sleep(){
        std::cout << name << " is sleeping." << std::endl;
    };
    void talk(){
        std::cout << name << " is talking." << std::endl;
    };

    void drink(){
        std::cout << name << " is drinking." << std::endl;
    };

};

int main() {
    // Create instance of Human class and set properties
    Human human1;
    human1.name = "John Doe";
    human1.age = 30;
    human1.gender = "Male";
    //print name  age and gender
    std::cout << "Name: " << human1.name << ", Age: " << human1.age << ", Gender: " << human1.gender << std::endl;


    human1.eat();
    human1.sleep();
    human1.talk();
    human1.drink();

    


    return 0;
}