#include <iostream>


//inheritance
class Animal {
    public:
    Animal() {
        std::cout << "Animal constructor called\n";
    
}
};

class Dog : public Animal {
    public:
    Dog() {
        std::cout << "Dog constructor called\n";
    }
    void bark() {
        std::cout << "Woof\n";
    }
};

int main() {

    Dog d;
    d.bark();

    return 0; 
}