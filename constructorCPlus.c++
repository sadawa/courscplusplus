#include <iostream>

class Student {
    public:
    std::string name;
    int age;
    double gpa;
    std::string description;

    // Constructor
    Student(std::string name, int age, double gpa){
        this->name = name;
        this->age = age;
        this->gpa = gpa;
    }
    Student(std::string name, int age, double gpa , std::string description){
        this->name = name;
        this->age = age;
        this->gpa = gpa;
        this->description = description;
    }



};

int main(){


    Student student1("John doe", 21 , 3.5);
    Student student2("Jimmy",20,3.2);
    Student student3("Sarah", 19, 3.7, "This is Sarah's description");

    std::cout << "Name: " << student1.name << std::endl;
    std::cout << "Age: " << student1.age << std::endl;
    std::cout << "GPA: " << student1.gpa << std::endl;

    std::cout << "Name: " << student2.name << std::endl;
    std::cout << "Age: " << student2.age << std::endl;
    std::cout << "GPA: " << student2.gpa << std::endl;

    std::cout << "Name: " << student3.name << std::endl;
    std::cout << "Age: " << student3.age << std::endl;
    std::cout << "GPA: " << student3.gpa << std::endl;
    std::cout << "Description: " << student3.description << std::endl;

    return 0;
}