#include<iostream>

int main() {
    //Type conversion Implicit conversion
    int a = 10;
    double b = a; // Implicit conversion from int to double
    
    //Type conversion Explicit conversion
    int c = 10;
    double d = static_cast<double>(c); // Explicit conversion from int to double

    std::cout << "Implicit conversion: " << b << std::endl;
    std::cout << "Explicit conversion: " << d << std::endl;
}