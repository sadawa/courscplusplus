#include <iostream>

//Use namespace
namespace first {
    int age = 25;
}

// Use namespace
namespace second {
    int age = 30;
}

// using namespace
using namespace first;

// using Typedef
typedef int age_t;

// Typedef with using
using text_t = std::string;
using number_t = int
;
int main() {
    // Variable

    // Int for integer (nombre entier)
    int age = 25;
    
    // Float for float (nombre à virgule flottante)
    float days =  5.2;
    
    // Char for character (caractère unique)
    char gender = 'M';
    
    // String for string (chaîne de caractères)
    std::string name = "John";

    // Double for double (nombre a virgule)
    double height = 1.80;

    // Bool for boolean (Vrai ou Faux)
    bool isStudent = true;

    //Const variables (ne peut pas etre changer)
    const int MAX_AGE = 100;

    // use typedef
    age_t age_typed = 26;
    text_t firstname = "John";
    number_t number = 10;
    
    std::cout << "I am " << age << " years old." << std::endl;
    std::cout << "I have been alive for " << days << " days." << std::endl;
    std::cout << "I am a " << gender << "." << std::endl;
    std::cout << "My name is " << name << "." << std::endl;
    std::cout << "I am " << height << " meters tall." << std::endl;
    std::cout << "Am I a student? " << (isStudent ? "Yes" : "No") << std::endl;
    std::cout << "The maximum age is " << MAX_AGE << "." << std::endl;
    std::cout << first::age << std::endl;
    std::cout << second::age << std::endl;
    std::cout << age_typed << std::endl;
    std::cout << firstname << std::endl;
    std::cout << number << std::endl;

    return 0;

}