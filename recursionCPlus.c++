#include <iostream>


void walk(int steps);
int main() {
    // incursion
    walk(25);

    return 0;
}

void walk(int steps) {
    //incursion
    // for(int i = 0; i < steps; i++){
    //     std::cout << "Walking... " << i << " steps" << std::endl;
    // }

    // Or 

    // recursion
    if(steps > 0){
        std::cout << "Walking... " << steps << " steps" << std::endl;
        walk(steps - 1);
    }
}