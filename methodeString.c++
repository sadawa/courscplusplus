#include <iostream>

int main() {
    // useful string methods c++
    std::string str = "Hello, World!";
    
    // length() method
    std::cout << "Length of string: " << str.length() << std::endl;
    
    // compare() method
    std::string str1 = "Hello";
    std::string str2 = "Hello";
    std::cout << "Comparison: " << (str1.compare(str2) == 0? "Equal" : "Not Equal") << std::endl;
    
    // substr() method
    std::string subStr = str.substr(6, 5);
    std::cout << "Substring: " << subStr << std::endl;
    
    // replace() method
    std::string replacedStr = str.replace(6, 5, "C++");
    std::cout << "Replaced string: " << replacedStr << std::endl;
    
    // find() method
    std::size_t found = str.find("World");
    if (found != std::string::npos) {
        std::cout << "World found at index: " << found << std::endl;

    }

    // rfind() method
    found = str.rfind("World");
    if (found!= std::string::npos) {
        std::cout << "World found at reverse index: " << found << std::endl;
    }

    // empty() method
    if (str.empty()) {
        std::cout << "String is empty" << std::endl;
    }

    // insert() method
    str.insert(6, "C++");
    std::cout << "Inserted string: " << str << std::endl;
    
    // append() method
    str.append(" is cool!");
    std::cout << "Appended string: " << str << std::endl;
    
    // erase() method
    std::string erasedStr = str.erase(6, 5);
    std::cout << "Erased string: " << erasedStr << std::endl;
    
    // to_upper() method
    std::string upperStr = str.substr(0, 6).to_upper() + str.substr(6);
    std::cout << "Uppercase string: " << upperStr << std::endl;
    
    // to_lower() method
    std::string lowerStr = str.substr(0, 6).to_lower() + str.substr(6);
    std::cout << "Lowercase string: " << lowerStr << std::endl;

    // concat() method
    std::string concatStr = str.substr(0, 6) + " " + str.substr(6);
    std::cout << "Concatenated string: " << concatStr << std::endl;

    // clear() method
    str.clear();
    
    std::cout << "String after clear(): " << str << std::endl;



    return 0;
}