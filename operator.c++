#include <iostream>

int main() {
   // Operator +
   int a = 5;
   int b = 3;
   int c = 10;
   int d = 2;

   int sum = a + b;
   int sum2 = c + d;
   std::cout << "Sum of a and b: " << sum << std::endl;
   std::cout << "Sum of c and d: " << sum2 << std::endl;

   // Operator -
   int subtraction = a - b;
   int subtraction2 = c - d;
   std::cout << "Subtraction of a and b: " << subtraction << std::endl;
   std::cout << "Subtraction of c and d: " << subtraction2 << std::endl;

   // Operator *
   int multiplication = a * b;
   int multiplication2 = c * d;
   std::cout << "Multiplication of a and b: " << multiplication << std::endl;
   std::cout << "Multiplication of c and d: " << multiplication2 << std::endl;

   // Operator /
   int division = a / b;
   int division2 = c / d;
   std::cout << "Division of a and b: " << division << std::endl;
   std::cout << "Division of c and d: " << division2 << std::endl;
   
   
   return 0;
}