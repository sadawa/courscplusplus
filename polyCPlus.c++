#include <iostream>


class Animal {
public:
    virtual void parler() { // Méthode virtuelle
        std::cout << "L'animal fait un bruit." << std::endl;
    }
};

class Chat : public Animal {
public:
    void parler() override {
        std::cout << "Le chat miaule." << std::endl;
    }
};

class Chien : public Animal {
public:
    void parler() override {
        std::cout << "Le chien aboie." << std::endl;
    }
};

int main() {
    Animal* animal1 = new Chat();
    Animal* animal2 = new Chien();

    animal1->parler(); 
    animal2->parler(); 
 

    return 0;
}