#include <iostream>

int main() {
    //Memory address and access with & operator
    int x = 5;
    std::cout << "Memory address of x: " << &x << std::endl;
    std::cout << "Value of x: " << x << std::endl;

    return 0;
}