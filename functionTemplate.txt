Les templates de fonction en C++ permettent de définir une fonction générique qui peut travailler avec des types de données différents sans écrire plusieurs versions de la même fonction.

Un template de fonction est une manière de créer une fonction généralisée, où le type des paramètres ou du retour peut être spécifié au moment de l'appel.

Pourquoi utiliser des Function Templates ?
Réduction du code redondant :

Pas besoin de définir plusieurs fonctions pour différents types de données.
Flexibilité :

La même fonction peut être utilisée avec des types de données différents (int, float, string, etc.).
Lisibilité et maintenabilité :

Le code est plus compact et facile à lire.
Syntaxe d’un Function Template

template <typename T>
retour nomFonction(T parametre) {
    // Corps de la fonction
}
template : Mot-clé qui introduit le template.
typename T : Déclare un type générique T. (On peut aussi utiliser class T, c’est équivalent.)
T : Type générique utilisé dans les paramètres ou dans le corps de la fonction.
Exemple 1 : Fonction générique pour trouver le maximum

#include <iostream>

// Définition d'un template de fonction
template <typename T>
T max(T a, T b) {
    return (a > b) ? a : b;
}

int main() {
    std::cout << "Max entre 10 et 20 : " << max(10, 20) << std::endl;         // int
    std::cout << "Max entre 3.14 et 2.71 : " << max(3.14, 2.71) << std::endl; // double
    std::cout << "Max entre 'a' et 'z' : " << max('a', 'z') << std::endl;     // char
    return 0;
}
Résultat :

mathematica
Copier le code
Max entre 10 et 20 : 20
Max entre 3.14 et 2.71 : 3.14
Max entre 'a' et 'z' : z
Explications :

Le type T est remplacé automatiquement par le type des arguments lors de l'appel (int, double, char).
Exemple 2 : Fonction générique pour échanger deux valeurs

#include <iostream>

// Template de fonction pour échanger deux valeurs
template <typename T>
void echanger(T& a, T& b) {
    T temp = a;
    a = b;
    b = temp;
}

int main() {
    int x = 10, y = 20;
    std::cout << "Avant l'échange : x = " << x << ", y = " << y << std::endl;
    echanger(x, y);
    std::cout << "Après l'échange : x = " << x << ", y = " << y << std::endl;

    double p = 3.14, q = 2.71;
    std::cout << "Avant l'échange : p = " << p << ", q = " << q << std::endl;
    echanger(p, q);
    std::cout << "Après l'échange : p = " << p << ", q = " << q << std::endl;

    return 0;
}
Résultat :


Avant l'échange : x = 10, y = 20
Après l'échange : x = 20, y = 10
Avant l'échange : p = 3.14, q = 2.71
Après l'échange : p = 2.71, q = 3.14
Explications :

La fonction echanger fonctionne pour tout type de données grâce au type générique T.
Exemple 3 : Fonction avec plusieurs types génériques
Un template peut également avoir plusieurs types génériques.


#include <iostream>

// Template de fonction avec deux types génériques
template <typename T, typename U>
void afficherDeuxValeurs(T a, U b) {
    std::cout << "Valeur 1 : " << a << ", Valeur 2 : " << b << std::endl;
}

int main() {
    afficherDeuxValeurs(10, 3.14);       // int et double
    afficherDeuxValeurs("Bonjour", 42); // string et int
    return 0;
}
Résultat :


Valeur 1 : 10, Valeur 2 : 3.14
Valeur 1 : Bonjour, Valeur 2 : 42
Explications :

T et U sont deux types génériques différents.
Spécialisation d’un template
Il est parfois nécessaire de créer une version spécifique d’un template pour un type de données particulier.

Exemple :

#include <iostream>

// Template générique
template <typename T>
void afficher(T valeur) {
    std::cout << "Valeur générique : " << valeur << std::endl;
}

// Spécialisation pour `char`
template <>
void afficher<char>(char valeur) {
    std::cout << "Valeur spécifique (char) : " << valeur << std::endl;
}

int main() {
    afficher(42);       // Appelle le template générique
    afficher('a');      // Appelle la spécialisation
    afficher(3.14);     // Appelle le template générique
    return 0;
}
Résultat :

arduino
Copier le code
Valeur générique : 42
Valeur spécifique (char) : a
Valeur générique : 3.14
Explications :

La spécialisation du template est utilisée pour les char, alors que les autres types utilisent le template générique.
Avantages des Function Templates
Réduction du code redondant :
Pas besoin de créer plusieurs fonctions pour différents types de données.
Flexibilité :
Peut être utilisé avec tout type de données, même des types définis par l'utilisateur.
Compatibilité :
S’intègre bien avec d’autres concepts avancés comme les conteneurs de la STL.
Limites des Function Templates
Augmentation de la complexité de compilation :
Le compilateur doit générer des versions spécifiques pour chaque type utilisé.
Difficulté de débogage :
Les messages d’erreur liés aux templates peuvent être complexes.
Spécialisation requise :
Nécessite parfois une spécialisation pour gérer des types particuliers.
Résumé
Aspect	Détail
Définition	Une fonction générique qui peut fonctionner avec des types différents.
Syntaxe	template <typename T> suivi d’une fonction utilisant T.
Usage	Réduction du code redondant, flexibilité pour différents types de données.
Exemples typiques	Fonctions pour échanger des valeurs, trouver des maximums, afficher des données.
Spécialisation	Permet de créer des versions spécifiques pour certains types si nécessaire.
