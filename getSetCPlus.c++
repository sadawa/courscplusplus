#include <iostream>

class Stove{
    private:
    int temperature = 0;


    public:
    int getTemperature(){
        return temperature;
    }

    void setTemperature(int temperature){
        this->temperature = temperature;
    }
};


int main() {

    Stove st;
    // st.temperature = 200;

    st.setTemperature(200);

    std::cout << "Stove temperature: " << st.getTemperature() << " degrees Celsius" << std::endl;


    return 0;
}