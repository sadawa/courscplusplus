#include <iostream>

int main() {
    // Use Dynamic Memory

    int *pNum = NULL;

    pNum = new int; // allocate memory

    *pNum = 26;


    std::cout << "address:"<< pNum << std::endl;
    std::cout << "Value of pNum: " << *pNum << std::endl;

    delete pNum; // deallocate memory

    return 0;
}