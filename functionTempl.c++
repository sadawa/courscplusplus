#include <iostream>

// We can add more templates here
template <typename T , typename U>

// tempalte function
// We can use auto 
T max(T x , T y){
    return (x > y)? x : y;
}

int main() {

    std::cout << max(1,2) << '\n';

    return 0;
}