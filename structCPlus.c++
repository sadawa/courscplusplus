#include <iostream>


//struct
    struct Employee {
        std::string name;
        int id;
        double salary;
    };

 // Create struct for car
 struct Car {
        std::string make;
        std::string model;
        int year;
    };

// Function to print car information
    void printCarInfo(Car car) {
        std::cout << "Make: " << car.make << std::endl;
        std::cout << "Model: " << car.model << std::endl;
        std::cout << "Year: " << car.year << std::endl;
    }


int main() {

// Use struc Employee
Employee emp1;

emp1.name = "John Doe";
    emp1.id = 123;
    emp1.salary = 50000.0;

    std::cout << "Employee Name: " << emp1.name << std::endl;
    std::cout << "Employee ID: " << emp1.id << std::endl;
    std::cout << "Employee Salary: $" << emp1.salary << std::endl;

    // Use fonction printCarInfo
    Car myCar;
    myCar.make = "Toyota";
    myCar.model = "Camry";
    myCar.year = 2020;
    printCarInfo(myCar);


    return 0;
}