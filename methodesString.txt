En C++, la classe std::string (définie dans l'en-tête <string>) fournit de nombreuses méthodes utiles pour manipuler les chaînes de caractères. Ces méthodes couvrent des fonctionnalités comme la recherche, la modification, la comparaison, la concaténation, et plus encore.

Liste complète des méthodes std::string
1. Création et modification
append : Ajoute une chaîne ou un caractère à la fin.
insert : Insère une sous-chaîne ou un caractère à une position donnée.
erase : Supprime une partie ou l’ensemble de la chaîne.
replace : Remplace une partie de la chaîne par une autre chaîne.
push_back : Ajoute un caractère à la fin.
pop_back : Supprime le dernier caractère.
clear : Vide complètement la chaîne.
resize : Change la taille de la chaîne (ajoute ou retire des caractères).
2. Accès aux caractères
at : Accède à un caractère à une position donnée (vérifie les limites).
operator[] : Accède à un caractère (ne vérifie pas les limites).
front : Retourne le premier caractère.
back : Retourne le dernier caractère.
3. Informations sur la chaîne
size / length : Retourne la longueur de la chaîne.
capacity : Retourne la capacité allouée pour la chaîne.
empty : Vérifie si la chaîne est vide.
max_size : Retourne la taille maximale possible de la chaîne.

4. Recherche
find : Trouve la première occurrence d’une sous-chaîne ou d’un caractère.
rfind : Trouve la dernière occurrence d’une sous-chaîne ou d’un caractère.
find_first_of : Trouve le premier caractère correspondant à un ensemble donné.
find_last_of : Trouve le dernier caractère correspondant à un ensemble donné.
find_first_not_of : Trouve le premier caractère qui ne correspond pas à un ensemble donné.
find_last_not_of : Trouve le dernier caractère qui ne correspond pas à un ensemble donné.

5. Comparaison
compare : Compare deux chaînes de caractères.

6. Extraction
substr : Extrait une sous-chaîne.

7. Conversion
c_str : Retourne un pointeur vers un tableau const char* (pour compatibilité avec C).
data : Semblable à c_str, mais retourne un tableau non null-terminé.

8. Autres
swap : Échange le contenu de deux chaînes.
operator+= : Ajoute une chaîne ou un caractère à la fin (concaténation).
operator+ : Concatène deux chaînes.
operator==, operator!=, etc. : Opérateurs de comparaison entre chaînes.
Exemples avec les méthodes std::string
1. Création et modification
append

#include <iostream>
#include <string>

int main() {
    std::string s = "Bonjour";
    s.append(" tout le monde");
    std::cout << s << std::endl; // Bonjour tout le monde
    return 0;
}
insert

#include <iostream>
#include <string>

int main() {
    std::string s = "Hello World!";
    s.insert(6, "beautiful ");
    std::cout << s << std::endl; // Hello beautiful World!
    return 0;
}
erase

#include <iostream>
#include <string>

int main() {
    std::string s = "Hello beautiful World!";
    s.erase(6, 10); // Supprime 10 caractères à partir de la position 6
    std::cout << s << std::endl; // Hello World!
    return 0;
}
replace

#include <iostream>
#include <string>

int main() {
    std::string s = "Hello World!";
    s.replace(6, 5, "Universe");
    std::cout << s << std::endl; // Hello Universe!
    return 0;
}
2. Accès aux caractères
at et operator[]

#include <iostream>
#include <string>

int main() {
    std::string s = "Hello";
    std::cout << s.at(1) << std::endl; // e
    std::cout << s[4] << std::endl;    // o
    return 0;
}
front et back

#include <iostream>
#include <string>

int main() {
    std::string s = "Hello";
    std::cout << s.front() << std::endl; // H
    std::cout << s.back() << std::endl;  // o
    return 0;
}
3. Recherche
find et rfind

#include <iostream>
#include <string>

int main() {
    std::string s = "C++ is fun and C++ is powerful";
    std::cout << s.find("C++") << std::endl;  // 0
    std::cout << s.rfind("C++") << std::endl; // 16
    return 0;
}
find_first_of et find_last_of

#include <iostream>
#include <string>

int main() {
    std::string s = "Hello World!";
    std::cout << s.find_first_of("aeiou") << std::endl; // 1 (premier 'e')
    std::cout << s.find_last_of("aeiou") << std::endl;  // 7 (dernier 'o')
    return 0;
}
4. Comparaison
compare

#include <iostream>
#include <string>

int main() {
    std::string s1 = "Hello";
    std::string s2 = "World";

    if (s1.compare(s2) == 0) {
        std::cout << "Les chaînes sont égales" << std::endl;
    } else {
        std::cout << "Les chaînes sont différentes" << std::endl;
    }

    return 0;
}
5. Extraction
substr

#include <iostream>
#include <string>

int main() {
    std::string s = "Hello World!";
    std::string sub = s.substr(6, 5); // Extrait 5 caractères à partir de la position 6
    std::cout << sub << std::endl; // World
    return 0;
}
6. Conversion
c_str

#include <iostream>
#include <string>

int main() {
    std::string s = "Hello";
    const char* c = s.c_str();
    std::cout << c << std::endl; // Hello
    return 0;
}
Quand utiliser ces méthodes ?
Manipulation de texte :

Ajouter, supprimer ou modifier des parties d'une chaîne.
Exemple : Formater une phrase dynamique avec append ou replace.
Recherche de données :

Trouver une sous-chaîne ou un caractère spécifique.
Extraction de parties :

Extraire une sous-chaîne avec substr.
Interopérabilité avec le C :

Utiliser c_str pour travailler avec des fonctions qui nécessitent const char*.