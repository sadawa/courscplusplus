#include <iostream>

int main() {
    //Pointers

    std::string name = "John";
    int age = 25;

    // nullptr
    int *pointers = nullptr;
    int x = 123;
    pointers = &x;

    if(pointers == nullptr){
        std::cout << "Pointer is null" << std::endl;
        
    } else {
        std::cout << "Pointer is not null" << std::endl;
        std::cout << "Value: " << *pointers << std::endl;
    }

    std::string *pName = &name;
    int *pAge = &age;

    std::cout << "Name: " << *pName << std::endl;
    std::cout << "Age: " << *pAge << std::endl;

    return 0;
}