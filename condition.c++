#include<iostream>

int main() {
    //condition if statement
    int age;

    std::cout << "Enter your age: ";
    std::cin >> age;

    if(age>=18){
        std::cout << "You are an adult.\n";
    } else if(age < 0 ) {
        std::cout << "Invalid age.\n";
    }
    else if(age >= 100){
        std::cout << "You are too old.\n";
    }
    
    else {
        std::cout << "You are a minor.\n";
    }

    // condition switch statement
    int month;
    std::cout << "Enter a month number (1-12): ";
    std::cin >> month;

    switch(month){
        case 1:
        std::cout << "January";
        break;
        case 2:
        std::cout << "February";
        break;
        case 3:
        std::cout << "March";
        break;
        case 4:
        std::cout << "April";
        break;
        case 5:
        std::cout << "May";
        break;
        case 6:
        std::cout << "June";
        break;
        case 7:
        std::cout << "July";
        break;
        case 8:
        std::cout << "August";
        break;
        case 9:
        std::cout << "September";
        break;
        case 10:
        std::cout << "October";
        break;
        case 11:
        std::cout << "November";
        break;
        case 12:
        std::cout << "December";
        break;
        default:
        std::cout << "Invalid month number.";
        break;
    }

    return 0;
}