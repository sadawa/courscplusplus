#include <iostream>

int main() {
    // ternary operator
    int x = 5;
    int y = 10;
    int z = (x > y)? x : y;
    std::cout << z << std::endl;

    // another example
    int a = 10;
    int b = 20;
    int c = (a < b)? a : b;
    std::cout << c << std::endl;

    // nested ternary operator
    int d = 5;
    int e = 10;
    int f = (d > e)? ((d % 2 == 0)? d : d + 1) : ((e % 2 == 0)? e : e + 1);
    std::cout << f << std::endl;

    //another example
    bool hungry = true;
    hungry ? std::cout << " You are hungry" : std::cout << "You are full" << std::endl;
    return 0;
}